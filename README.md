# VR Tour Example

A small part of the FEFU virtual reality tour created for the admission committee.  Includes full source project code and several locations. Allows you to view photos and videos 360, as well as 2D video. 
The project was created on the A-Fram framework using the Javascript language. The finished tour can be seen at the address: vrfefu360.ru
