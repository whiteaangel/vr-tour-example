var newPoint;

let imgNew = [];
let spotOld =[];
let spotNew =[];

imgNew[0] = "#MainEntrance";
var historyDepth = 10;

var i = 0;
var j = 0;


function SaveDataToLocalStorage(data) {
	var a;
	if (localStorage.getItem('history') === null) {
		a = [];
	} else {
		a = JSON.parse(localStorage.getItem('history'));
	}
	if (a.length === historyDepth) {
		a.shift();
	}
	a.push(data);
	localStorage.setItem('history', JSON.stringify(a));
}

$(document).ready(function() {
	var $videoSrc;
	$(".video-btn").click(function() {
	 	$videoSrc = $(this).data( "src" );
	});

	$("#myModal").on('shown.bs.modal', function (e) {
		$("#video").attr('src', $videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
	});

	$("#myModal").on('hide.bs.modal', function (e) {
		$("#video").attr('src', $videoSrc);
	});
});


function BackToPhoto() {
	var videoSphere = document.getElementById("videosphere");
	var sky = document.getElementById("skybox");
	var spots = document.getElementById("spots");

	var homeButton = document.getElementById("homebutton");
	var backbutton = document.getElementById("back360VideoButton");

	if (videoSphere.getAttribute("visible") == true) {
		videoSphere.setAttribute("visible", "false");
		videoSphere.components.material.material.map.image.pause();

		backbutton.disabled = 1;
		backbutton.style.display = "none";

		homeButton.disabled = 0;
		homeButton.style.display = "block";

		sky.setAttribute("visible", "true");
		spots.setAttribute("visible","true");
		spots.object3D.scale.set(1, 1, 1);
	}
}



function GoHome() {
	var url = "index.html";
	document.location.href = url;
}


function back() {
	h = JSON.parse(localStorage.getItem('history'));
	var url = h.pop() || { 'href': 'index.html' };
	document.location.href = url['href'];
	localStorage.setItem('history', JSON.stringify(h));
}


AFRAME.registerComponent('hotspot', {
	init:function() {
		this.el.addEventListener('reloadspots',function(evt) {
			var currspotgroup = document.getElementById(evt.detail.currspots);
			currspotgroup.setAttribute("visible","false");
			currspotgroup.object3D.scale.set(0, 0, 0);

			spotOld.push(currspotgroup);

			var newspotgroup = document.getElementById(evt.detail.newspots);
			newspotgroup.setAttribute("visible", "true");
			newspotgroup.object3D.scale.set(1, 1, 1);

			spotNew.push(newspotgroup);
			i++;

			newPoint = newspotgroup;
		});
	}
});


AFRAME.registerComponent('spot', {
	schema: {
		linkto: { type: "string", default: "" },
		source: { type: "string", default: "" }
	},
	init: function() {
		var data = this.data;
		this.el.setAttribute("src", data.source);
		this.el.setAttribute("look-at", "#cam");
		this.el.addEventListener('click',function() {
			SaveDataToLocalStorage(window.location);
			var sky = document.getElementById("skybox");
			sky.setAttribute("src", data.linkto);
			document.location.href = data.linkto;
		});

		this.el.addEventListener('mouseenter',function() {
			this.children[0].setAttribute("visible", "true");
		});

		this.el.addEventListener('mouseleave', function() {
			this.children[0].setAttribute("visible", "false");
		});
	}
});


AFRAME.registerComponent('mspot', {
	schema: {
		source: { type: "string", default: "#Education" }
	},
	init: function() {
		var data = this.data;
		let obj = new Array();
		var checkForVisible = 0;

		this.el.setAttribute("src", data.source);
		this.el.setAttribute("look-at", "#cam");

		function WaitForEnd() {
			if (checkForVisible === 0) {
				for(let i = 1; i < obj.length; i++ ) {
					obj[i].setAttribute("visible", "false");
				}
			}
			setTimeout(WaitForEnd, 8000);
		}

		this.el.addEventListener('click', function() {
			for (let i = 1; i < this.children.length; i++ ) {
				this.children[i].setAttribute("visible", "true");
				obj[i] = this.children[i];
			}
			setTimeout(WaitForEnd, 8000);
		});

		this.el.addEventListener('mouseenter', function() {
			checkForVisible = 1;
			this.children[0].setAttribute("visible", "true");
		});

		this.el.addEventListener('mouseleave', function() {
			checkForVisible = 0;
			this.children[0].setAttribute("visible", "false");
		});
	}
});


AFRAME.registerComponent('spot_2', {
	schema: {
		linkto: { type: "string", default: "" },
		source: { type: "string", default: "" }
	},
	init: function() {
		var data=this.data;
		this.el.setAttribute("src", data.source); //add image source of hotspot icon
		this.el.setAttribute("look-at", "#cam"); //make the icon look at the camera all the time

		this.el.addEventListener('click', function() {
			SaveDataToLocalStorage(window.location);
			var url = data.linkto;
			document.location.href = url;
			console.log(url);
		});

		this.el.addEventListener('mouseenter',function() {
			this.children[0].setAttribute("visible", "true");
		});

		this.el.addEventListener('mouseleave', function() {
			this.children[0].setAttribute("visible", "false");
		});
	}
});


AFRAME.registerComponent('video360', {
	schema: {
		linkto: { type: "string", default: "" },
		source: { type: "string", default: "" }
	},
	init: function() {
		var data = this.data;
		this.el.setAttribute("src", data.source); //add image source of hotspot icon
		this.el.setAttribute("look-at", "#cam"); //make the icon look at the camera all the time

		function TextVisible() {
			var videoSphere = document.getElementById("videosphere");
			videoSphere.children[0].children[0].setAttribute("visible", "false");
		}

		function sleep(ms) {
			return new Promise(resolve => setTimeout(resolve, ms));
		}

		this.el.addEventListener('click', async function() {
			//set the skybox source to the new image as per the spot
			var homeButton = document.getElementById("homebutton");

			var backbutton = document.getElementById("back360VideoButton");

			var sky = document.getElementById("skybox");
			sky.setAttribute("visible", "false");

			var spots = document.getElementById("spots");
			spots.setAttribute("visible", "false");
			spots.object3D.scale.set(0, 0, 0);

			var videoSphere = document.getElementById("videosphere");
			videoSphere.setAttribute("src", data.linkto);

			var videoid = String(data.linkto);
			videoid = videoid.substring(1);
			var video = document.getElementById(videoid);

			homeButton.disabled = 1;
			homeButton.style.display = "none";

			videoSphere.setAttribute("visible", "true");

			videoSphere.components.material.material.map.image.load();
			videoSphere.components.material.material.map.image.play();

			videoSphere.children[0].children[0].setAttribute("visible", "true");
			setTimeout(TextVisible, 4000);

			backbutton.disabled = 0;
			backbutton.style.display = "block";
		});

		this.el.addEventListener('mouseenter',function() {
			this.children[0].setAttribute("visible", "true");
		});

		this.el.addEventListener('mouseleave',function() {
			this.children[0].setAttribute("visible", "false");
		});
	}
});


AFRAME.registerComponent('video2d', {
	schema: {
		source: { type: "string", default: "" }
	},
	init: function() {
		var data = this.data;
		this.el.setAttribute("src", data.source);
		this.el.setAttribute("look-at", "#cam");
		this.el.addEventListener('mouseenter', function() {
			this.children[0].setAttribute("visible", "true");
		});

		this.el.addEventListener('mouseleave',function() {
			this.children[0].setAttribute("visible", "false");
		});
	}
});


AFRAME.registerComponent('foto360', {
	init: function() {
		var videoSphere = document.getElementById("videosphere");
		document.addEventListener('keydown', function (event) {
			if (event.key === "Escape") {
				if (videoSphere.getAttribute("visible") == true) {
					videoSphere.setAttribute("visible", "false");
					videoSphere.components.material.material.map.image.pause();

					var sky = document.getElementById("skybox");
					sky.setAttribute("visible", "true");

					var spots = document.getElementById("spots");
					spots.setAttribute("visible", "true");
					spots.object3D.scale.set(1, 1, 1);
				}
			}
		});
	}
});